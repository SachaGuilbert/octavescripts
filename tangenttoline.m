%Example
t=-200:1:200;
y=t.^4 - 14*t.^2 + 24*t;
plot(t,y)
%-------------------------
dy=diff(y)./diff(t)
k=15; % point number 220
tang=(t-t(k))*dy(k)+y(k)
hold on
plot(t,tang)
scatter(t(k),y(k))
fontsize = 20;
%legend("f(x)")
hold off
