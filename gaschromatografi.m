x = [4,10,20,30];
y = [55.7,142,275,417];
A = polyfit(x,y,1);
x2 = [4:0.1:40];
y2 = x2*A(1) + A(2);
scatter(x,y)
hold on
plot(x2,y2)
hold off
xlabel("Ethanolindhold (%)");
ylabel("Areal");
